
* selenium serverless with python : https://medium.com/hackernoon/running-selenium-and-headless-chrome-on-aws-lambda-layers-python-3-6-bd810503c6c3

* chromedriver ; https://chromedriver.storage.googleapis.com/index.html

* serverless python packaging ; https://www.serverless.com/blog/serverless-python-packaging

* SNS and AWS Lambda ; https://docs.aws.amazon.com/sns/latest/dg/sns-lambda-as-subscriber.html